<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
    {
        public $timestamps = false;
        public $primaryKey= "id";
        protected $table = "books";
        protected $fillable = [
            'id','book_title', 'book_image','book_published', 'id_genre', 'id_author','id_publisher'
        ];
    }