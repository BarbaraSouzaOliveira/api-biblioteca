<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
    {
        public $timestamps = false;
        public $primaryKey= "id";
        protected $table = "authors";
        protected $fillable = [
            'id','author_name','author_born','author_nationality', 'author_genre'
        ];
    }