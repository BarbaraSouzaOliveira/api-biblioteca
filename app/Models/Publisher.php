<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
    {
        public $timestamps = false;
        public $primaryKey= "id";
        protected $table = "publishers";
        protected $fillable = [
            'id','publisher_name'
        ];
    }