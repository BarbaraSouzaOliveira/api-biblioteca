<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::get();
        return \response()->json($books);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Book;
        $pathfoto=null;
        if ($request->hasfile('book_image')) {
            if($book->book_image){
                $pathfoto = $book->book_image;
            }else{    
                $pathfoto = $request->book_image->store('public/uploads/photo');
            }
        }
        $book->book_title = $request->book_title;
        $book->book_image = $pathfoto;
        $book->book_published = $request->book_published;
        $book->id_author = $request->id_author;
        $book->id_genre = $request->id_genre;
        $book->id_publisher = $request->id_publisher;
        $book->save();
        return \response()->json($book);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::where('id', $id)->first();
        return \response()->json($book);
    }

    /** 
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $book = Book::find($id);        
        $pathfoto=null;
        if ($request->hasfile('book_image')) {
            if($book->book_image){
                $pathfoto = $book->book_image;
            }else{    
                $pathfoto = $request->book_image->store('public/uploads/photo');
            }
        }
        $book->book_title = $request->book_title;
        $book->book_image = $pathfoto;
        $book->book_published = $request->book_published;
        $book->id_author = $request->id_author;
        $book->id_genre = $request->id_genre;
        $book->id_publisher = $request->id_publisher;
        $book->save();
        return \response()->json($book);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book-> delete();
    }
}