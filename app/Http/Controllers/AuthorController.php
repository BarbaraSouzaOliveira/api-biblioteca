<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors = Author::get();
        return \response()->json($authors);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $author = new Author;
        $author->author_name = $request->author_name;
        $author->author_born = $request->author_born;
        $author->author_genre = $request->author_genre;
        $author->author_nationality = $request->author_nationality;
        $author->save();
        return \response()->json($author);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $author = author::where('id', $id)->first();
        return \response()->json($author);
    }

    /** 
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $author = Author::find($id);        
        $author->author_name = $request->author_name;
        $author->author_born = $request->author_born;
        $author->author_genre = $request->author_genre;
        $author->author_nationality = $request->author_nationality;
        $author->save();
        return \response()->json($author);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::find($id);
        $author-> delete();
    }
}