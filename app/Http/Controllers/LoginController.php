<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use \Firebase\JWT\JWT;

class LoginController extends Controller
{
    public function login (Request $request){        
        $login = Usuario::where('email', $request->email)->where('senha', $request->senha)->first();
        $key = "example_key";
        $payload = array(
            "iss" => "http://example.org",
            "aud" => "http://example.com",
            "iat" => 1356999524,
            "nbf" => 1357000000,
            "data" => $login
        );

            /**
             * IMPORTANT:
             * You must specify supported algorithms for your application. See
             * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
             * for a list of spec-compliant algorithms.
             */
        $jwt = JWT::encode($payload, $key);
        return $jwt;
    }

    
}