<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use \Firebase\JWT\JWT;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $key = "example_key";
        $token = $request->header('Authorization');
        $token = str_replace('Bearer ', '', $token);
        $decoded = JWT::decode($token, $key, array('HS256'));
        $request->offsetSet('usuario', $decoded->data);
        return $next($request);
    }
}
