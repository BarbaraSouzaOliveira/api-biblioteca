<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




/*--------------------------------------------------------------------------
/User Routes 
---------------------------------------------------------------------------*/
Route::get('/usuario','UsuarioController@index');
Route::get('/usuario/{id}','UsuarioController@show');
Route::post('/usuario','UsuarioController@store');
Route::post('/usuario/{id}','UsuarioController@update');
Route::delete('/usuario/{id}','UsuarioController@destroy');

Route::post('/login', 'LoginController@login');


Route::middleware(['authjwt'])->group(function () { 

/*--------------------------------------------------------------------------
/Genre Routes 
---------------------------------------------------------------------------*/
Route::get('/genre','GenreController@index');
Route::get('/genre/{id}','GenreController@show');
Route::post('/genre','GenreController@store');
Route::put('/genre/{id}','GenreController@update');
Route::delete('/genre/{id}','GenreController@destroy');

/*--------------------------------------------------------------------------
/Publisher Routes 
---------------------------------------------------------------------------*/
Route::get('/publisher','PublisherController@index');
Route::get('/publisher/{id}','PublisherController@show');
Route::post('/publisher','PublisherController@store');
Route::put('/publisher/{id}','PublisherController@update');
Route::delete('/publisher/{id}','PublisherController@destroy');

/*--------------------------------------------------------------------------
/Author Routes 
---------------------------------------------------------------------------*/
Route::get('/author','AuthorController@index');
Route::get('/author/{id}','AuthorController@show');
Route::post('/author','AuthorController@store');
Route::put('/author/{id}','AuthorController@update');
Route::delete('/author/{id}','AuthorController@destroy');

/*--------------------------------------------------------------------------
/Book Routes 
---------------------------------------------------------------------------*/
Route::get('/book','BookController@index');
Route::get('/book/{id}','BookController@show');
Route::post('/book','BookController@store');
Route::post('/book/{id}','BookController@update');
Route::delete('/book/{id}','BookController@destroy');

});