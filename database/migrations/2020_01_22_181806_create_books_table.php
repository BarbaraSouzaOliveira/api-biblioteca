<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('book_title');
            $table->string('book_image')->nullable();
            $table->string('book_published');
            $table->unsignedBigInteger('id_author');            
            $table->unsignedBigInteger('id_publisher');
            $table->unsignedBigInteger('id_genre');            
            $table->foreign('id_author')->references('id')->on('authors');
            $table->foreign('id_publisher')->references('id')->on('publishers');
            $table->foreign('id_genre')->references('id')->on('genres');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
