


## Configuração da Máquina para o Back-end

Nota: Caso queira ler o readme pelo docs acesse o link https://docs.google.com/document/d/1KIEX28k-vplJsZ7383j9dA_H0xk-NXJZaGHwC5Axc6U/edit?usp=sharing/

## Especificações do projeto

Neste projeto foi usado o php 7.4.1 e a framework Laravel 6.2 
Banco de dados SQL Serve. 
Diagrama das tabelas https://drive.google.com/file/d/144eJvKLCriXK4BYOwu3DI0QElji0f4F9/view?usp=sharing 

## Chocolatey

Chocolatey é um gerenciador de pacotes para o windows. Sua instalação é bem simples, pelo powershell (em modo administrador) rode o comando Get-ExecutionPolicy  se retornar Restricted rode Set-ExecutionPolicy Bypass -Scope Process. 
Em seguida rode o comando 
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))  
 Após isso, o chocolatey estará instalado na sua máquina, finalize o powershell.

## Composer e Laravel

Abra o PowerShell em modo administrador rode o comando choco install composer.
Finalize o PowerShell e abra o novamente em modo administrador, rode o comando composer global require laravel/installer

## Repositório

Após clonar o repositório  em sua máquina na pasta do repositório rode o comando através do terminal Composer install.


## PHP Drives

Após clonar o repositório e fazer as instalações de dependencias com o composer install, vá até a pasta do php em sua máquina, por padrão fica em C:\tools\php74 , na pasta ”ext” copie e cole os dois drives que estão no link para download https://drive.google.com/drive/folders/1YtYI3-Wnm_EMiG9MG8jIGKzhsCyj5iAZ?usp=sharing , lembre-se de baixar o drive compatível com a versão do seu windows. Após colar os drives na pasta ext, abra o php.ini e verifique se
extension=pdo_sqlite
extension=openssl
extension=fileinfo
estão descomenetadas,  em
; Maximum allowed size for uploaded files.
adicione 
upload_max_filesize = 40M
em 
Must be greater than or equal to upload_max_filesize
adicione 
post_max_size = 40M
Em extension adicione 
extension=sqlsrv_74_nts
extension=pdo_sqlsrv_74_nts
Após as modificações salve, e reinicie a máquina.

## SQLServe

No site da microsoft faça o download do sql serve Developer https://www.microsoft.com/pt-br/sql-server/sql-server-downloads
e o sql studio 
https://docs.microsoft.com/pt-br/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15


## Habilitando usuário SA no SQLServer 

Para habilitar o usuário SA no SQL Server basta seguir este tutorial:
https://atendimento.redehost.com.br/hc/pt-br/articles/218105357-Habilitando-usu%C3%A1rio-SA-no-SQL-Server-2012

## Restaurando um banco de dados no SQLServer 

Para restaurar: No SQL Server Management Studio.
Botão direito sobre o item BANCO DE DADOS - > Restaurar -> Localizar o arquivo BAK e restaurar.
link do .bak = https://drive.google.com/open?id=1DWEnFXqQ39jTzN3l6ds4en4OjdY1ETHU
*restaurar o banco é opcional, caso faça um banco novo, basta configurar o .env na api como
será descrito e após todo processo antes de executar o serve rodar php artisan migrate
## Preenchendo o .env

Na pasta raiz do repositório crie um file .env e preencha da seguinte maneira:
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:hA76Yb1848Ln70UfxGzQQHP21Nyk1CNfvNA53FkT/Tk=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=sqlsrv
DB_HOST=*AQUI VOCÊ VAI COLOCAR O NOME DA SUA MÁQUINA, EM SOBRE O COMPUTADOR COPIE O NOME DO DISPOSITIVO E COLE AQUI, EX:DESKTOP-PXEQ01T*
DB_PORT=1433
DB_DATABASE=*NOME DO BANCO DE DADOS, EX: BIBLIOTECA”
DB_USERNAME=sa
DB_PASSWORD=*A SENHA QUE VOCÊ DEFINIU NA HORA QUE HABILITOU O SA*

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

## Configurações Laravel

Após todos os processos anteriores rode o comando php artisan key:generate  em seguida rode o comando  php artisan storage:link

## Comando para rodar o server

Para rodar o server rode o comando  php artisan serve

## Comando para migrate

Para dar migrate das tabelas rode php artisan migrate




<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)
- [Hyper Host](https://hyper.host)
- [Appoly](https://www.appoly.co.uk)
- [OP.GG](https://op.gg)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
